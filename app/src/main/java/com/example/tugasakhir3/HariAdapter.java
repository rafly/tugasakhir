//package com.example.tugasakhir3;
//
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.TextView;
//
//import androidx.annotation.NonNull;
//import androidx.recyclerview.widget.RecyclerView;
//import java.util.ArrayList;
//
//public class HariAdapter extends RecyclerView.Adapter<HariAdapter.HariViewHolder> {
//    private ArrayList<Hari> listHari;
//
//    public HariAdapter(ArrayList<Hari> listHari) {
//        this.listHari = listHari;
//    }
//
//    @NonNull
//    @Override
//    public HariViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
//        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_hari, parent, false);
//        return new HariViewHolder(view);
//    }
//
//    @Override
//    public void onBindViewHolder(@NonNull HariViewHolder holder, int position) {
//        Hari hari = listHari.get(position);
//        holder.tvDate.setText(hari.getDate());
//        holder.tvJudul.setText(hari.getJudul());
//        holder.tvText.setText(hari.getText());
//    }
//
//    @Override
//    public int getItemCount() {
//        return listHari.size();
//    }
//
//    public static class HariViewHolder extends RecyclerView.ViewHolder {
//        TextView tvDate, tvJudul, tvText;
//
//        public HariViewHolder(@NonNull View itemView) {
//            super(itemView);
//            tvDate = itemView.findViewById(R.id.tvDate);
//            tvJudul = itemView.findViewById(R.id.tvJudul);
//            tvText = itemView.findViewById(R.id.tvText);
//        }
//    }
//}
//
