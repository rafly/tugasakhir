package com.example.tugasakhir3;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class DiaryActivity extends AppCompatActivity {

    TextView diaryDay, diaryMonth, diaryYear, diaryJudul, diaryIsi;
    ImageButton btToMain;
    Button btSave;
    FirebaseDatabase database;
    DatabaseReference reference;
    FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_diary);
        diaryDay = findViewById(R.id.diaryDay);
        diaryMonth = findViewById(R.id.diaryMonth);
        diaryYear = findViewById(R.id.diaryYear);
        diaryJudul = findViewById(R.id.diaryJudul);
        diaryIsi = findViewById(R.id.diaryIsi);
        btToMain = findViewById(R.id.diaryButton_toMain);
        btSave = findViewById(R.id.diarySave);

        //Firebase
        firebaseAuth = FirebaseAuth.getInstance();
        FirebaseUser user = firebaseAuth.getCurrentUser();

        btToMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (user != null) {
                    database = FirebaseDatabase.getInstance();
                    reference = database.getReference("Users");

                    String uidUser = user.getUid();
                    String judul = diaryJudul.getText().toString();
                    String isi = diaryIsi.getText().toString();
                    String day = diaryDay.getText().toString();
                    String month = diaryMonth.getText().toString();
                    String year = diaryYear.getText().toString();

                    Diary diaryClass = new Diary(judul, isi, day, month, year);

                    // Referensi ke bulan pengguna
                    DatabaseReference userMonthRef = reference.child(uidUser).child(month);

                    userMonthRef.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            if (dataSnapshot.exists()) {
                                // Jika bulan sudah ada, tambahkan data diary
                                userMonthRef.push().setValue(diaryClass);
                            } else {
                                // Jika bulan belum ada, buat cabang baru dengan nama bulan
                                userMonthRef.setValue(""); // Membuat cabang bulan kosong
                                userMonthRef.push().setValue(diaryClass); // Menambahkan data diary ke bulan
                            }

                            Toast.makeText(DiaryActivity.this, "Data saved successfully!", Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                            Toast.makeText(DiaryActivity.this, "Failed to check month in the database.", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        });

    }
}