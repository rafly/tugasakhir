package com.example.tugasakhir3;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    ImageButton btToDiary;
    FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btToDiary = findViewById(R.id.mainButton_toDiary);

        // Di dalam onCreate
        RecyclerView recyclerViewBulan = findViewById(R.id.recyclerViewBulan);
        recyclerViewBulan.setLayoutManager(new LinearLayoutManager(this));
        ArrayList<Bulan> listBulan = new ArrayList<>();
        BulanAdapter bulanAdapter = new BulanAdapter(listBulan);
        recyclerViewBulan.setAdapter(bulanAdapter);

        // Ini buat nyambungin ke Firebase
        firebaseAuth = FirebaseAuth.getInstance();
        FirebaseUser user = firebaseAuth.getCurrentUser();

        if (user != null) {
            String uidUser = user.getUid();
            FirebaseDatabase database = FirebaseDatabase.getInstance();
            DatabaseReference bulanRef = database.getReference("Users").child(uidUser); // Gantilah sesuai dengan path yang sesuai di Firebase Database.

            bulanRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    listBulan.clear(); // Bersihkan list sebelum menambahkannya kembali.
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        String namaBulan = snapshot.getKey(); // Mengambil nama bulan sebagai key.
                        Bulan bulan = new Bulan(namaBulan);
                        listBulan.add(bulan);
                    }
                    bulanAdapter.notifyDataSetChanged(); // Notifikasi adapter tentang perubahan data.
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    // Handle kesalahan jika ada.
                }
            });
        }

        btToDiary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent toDiary = new Intent(MainActivity.this, DiaryActivity.class);
                startActivity(toDiary);
            }
        });
    }
}
