package com.example.tugasakhir3;

public class Diary {
    String judul, isi, day, month, year;

    public Diary(String judul, String isi, String day, String month, String year) {
        this.judul = judul;
        this.isi = isi;
        this.day = day;
        this.month = month;
        this.year = year;
    }

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public String getIsi() {
        return isi;
    }

    public void setIsi(String isi) {
        this.isi = isi;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }
}
