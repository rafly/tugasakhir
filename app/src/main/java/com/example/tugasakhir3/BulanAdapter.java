package com.example.tugasakhir3;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class BulanAdapter extends RecyclerView.Adapter<BulanAdapter.BulanViewHolder> {
    private ArrayList<Bulan> listBulan;

    public BulanAdapter(ArrayList<Bulan> listBulan) {
        this.listBulan = listBulan;
    }

    @NonNull
    @Override
    public BulanViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_bulan, parent, false);
        return new BulanViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BulanViewHolder holder, int position) {
        Bulan bulan = listBulan.get(position);
        holder.textViewBulan.setText(bulan.getNamaBulan());
    }

    @Override
    public int getItemCount() {
        return listBulan.size();
    }

    public static class BulanViewHolder extends RecyclerView.ViewHolder {
        TextView textViewBulan;

        public BulanViewHolder(@NonNull View itemView) {
            super(itemView);
            textViewBulan = itemView.findViewById(R.id.textViewBulan);
        }
    }
}

