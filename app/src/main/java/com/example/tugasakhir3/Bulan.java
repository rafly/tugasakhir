package com.example.tugasakhir3;

public class Bulan {
    private String namaBulan;

    public Bulan() {
        // Diperlukan konstruktor kosong untuk Firebase Realtime Database.
    }

    public Bulan(String namaBulan) {
        this.namaBulan = namaBulan;
    }

    public String getNamaBulan() {
        return namaBulan;
    }

    public void setNamaBulan(String namaBulan) {
        this.namaBulan = namaBulan;
    }
}
